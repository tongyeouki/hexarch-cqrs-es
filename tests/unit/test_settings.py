import os

import pytest

from settings import get_config, WrongEnvironmentError


class TestSettings:
    def test_testing_environment(self):
        os.environ["ENVIRONMENT"] = "test"
        assert get_config().__name__ == "TestConfig"

    def test_wrong_environment(self):
        os.environ["ENVIRONMENT"] = "azerty"
        with pytest.raises(WrongEnvironmentError) as error:
            get_config()
        assert '\nUnknown environment: "azerty".\n' in str(error.value)
