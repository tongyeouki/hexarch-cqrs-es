from unittest import mock

from books.domain import commands
from books.domain.book import Book
from books.domain.events import BookCreated, BookUpdated
from books.services.notifications import InMemoryEmailService


class TestNotifications:
    @mock.patch.object(InMemoryEmailService, "send")
    def test_create_book_should_notify_users(
        self, mocked_email_service, create_book_cmd
    ):
        Book.create(create_book_cmd)
        mocked_email_service.called_once_with(BookCreated)

    @mock.patch.object(InMemoryEmailService, "send")
    def test_create_book_should_not_notify_users(
        self, mocked_email_service, create_book_cmd
    ):
        book = Book.create(create_book_cmd)
        cmd = commands.UpdateBook(uuid=book.uuid, author="John Smith", title="Thoughts")
        book.update(cmd=cmd)
        mocked_email_service.called_once_with(BookUpdated)
