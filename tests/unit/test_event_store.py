import pytest

from books.domain import commands
from books.domain.book import Book
from event_store.exceptions import InvalidAggregateError, InvalidEventError
from event_store.services.mappers import AggregateMapper, EventsMapper
from event_store.infrastructure.repositories import InMemoryEventStore
from tests.conftest import FakeAggregate, FakeEvent


class TestDomainEvent:
    def test_domain_events_model(self):
        from datetime import datetime

        event = FakeEvent()
        assert isinstance(event.data, dict)
        assert isinstance(event.created_at, datetime)


class TestAggregateMapper:
    def test_should_register_aggregate_in_mapper(self):
        mapper = AggregateMapper()
        mapper.register(FakeAggregate)
        assert mapper.aggregates == {"FakeAggregate": FakeAggregate}

    def test_should_fail_to_register_aggregates_in_mapper(self):
        mapper = AggregateMapper()
        with pytest.raises(InvalidAggregateError) as error:
            mapper.register(FakeEvent)
            assert "Registered object should be an aggregate." in error.value

    def test_should_retrieve_aggregate(self):
        mapper = AggregateMapper()
        mapper.register(FakeAggregate)
        instance = mapper.get("FakeAggregate")
        assert instance == FakeAggregate

    def test_should_fail_to_retrieve_not_registered_aggregate(self):
        mapper = AggregateMapper()
        with pytest.raises(InvalidEventError):
            mapper.get("FakeAggregate")


class TestEventsMapper:
    def test_should_register_events_in_mapper(self):
        mapper = EventsMapper()
        mapper.register(FakeEvent)
        assert mapper.events == {"FakeEvent": FakeEvent}

    def test_should_fail_to_register_events_in_mapper(self):
        mapper = EventsMapper()
        with pytest.raises(InvalidEventError) as error:
            mapper.register(Book)
            assert "Registered object should be an Event." in error.value

    def test_should_retrieve_event(self):
        mapper = EventsMapper()
        mapper.register(FakeEvent)
        instance = mapper.get("FakeEvent")
        assert instance == FakeEvent

    def test_should_fail_to_retrieve_not_registered_event(self):
        mapper = EventsMapper()
        with pytest.raises(InvalidEventError):
            mapper.get("FakeEvent")


class TestEventStore:
    @pytest.fixture
    def book(self, create_book_cmd):
        book = Book.create(create_book_cmd)
        cmd = commands.UpdateBook(uuid=book.uuid, author="John Smith", title="Thoughts")
        book.update(cmd=cmd)
        return book

    @pytest.fixture
    def populated_event_store(self, book):
        event_store = InMemoryEventStore()
        event_store.append_to_stream(
            aggregate_id=book.uuid,
            originating_version=book.version,
            events=book.changes,
            aggregate_name=book.__class__.__name__,
        )
        return event_store

    def test_should_append_to_stream(self, populated_event_store):
        assert populated_event_store.stored_events is not None

    def test_should_load_event_stream(self, book, populated_event_store):
        aggregate = populated_event_store.load_stream(aggregate_id=book.uuid)
        assert aggregate.event_stream.events == book.event_stream.events
        assert aggregate.event_stream.version == book.event_stream.version + 1
