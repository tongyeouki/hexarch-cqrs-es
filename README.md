# Hexarch-CQRS-ES - The bookstore case

[![pipeline status](https://gitlab.com/p2m3ng/hexarch-cqrs-es/badges/master/pipeline.svg)](https://gitlab.com/p2m3ng/hexarch-cqrs-es/-/commits/master)
[![coverage report](https://gitlab.com/p2m3ng/hexarch-cqrs-es/badges/master/coverage.svg)](https://gitlab.com/p2m3ng/hexarch-cqrs-es/-/commits/master)

## Hexagonal Architecture

## CQRS

https://martinfowler.com/bliki/CQRS.html

> At its heart is the notion that you can use a different model to warn 
>information than the model you use to read information.
