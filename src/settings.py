import abc
import os
import sys

from dotenv import load_dotenv

from books.services.unit_of_work import InMemoryUnitOfWork


class WrongEnvironmentError(Exception):
    pass


class Config(abc.ABC):
    pass


class TestConfig(Config):
    uow = InMemoryUnitOfWork()


def get_config():
    try:
        load_dotenv(".env")
        environment = os.environ["ENVIRONMENT"]
    except KeyError:  # pragma: no cover
        sys.tracebacklimit = 0  # pragma: no mutate
        raise WrongEnvironmentError("Setup .env file with appropriate parameters.")

    if environment == "test":
        return TestConfig
    else:
        sys.tracebacklimit = 0  # pragma: no mutate
        raise WrongEnvironmentError(
            f'\nUnknown environment: "{environment}".'
            f"\nSetup appropriate configuration in {__file__}"
        )
