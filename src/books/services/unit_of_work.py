import abc

from books.adapters.secondaries import repositories
from event_store.infrastructure.repositories import IEventStore, InMemoryEventStore


class UnitOfWork(abc.ABC):  # pragma: no cover
    books: repositories.BookRepository
    event_store: IEventStore

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.rollback()

    def commit(self):
        self._commit()

    def rollback(self):
        self._rollback()

    @abc.abstractmethod
    def _commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    def _rollback(self):
        raise NotImplementedError


class InMemoryUnitOfWork(UnitOfWork):
    def __init__(self):
        self.books = repositories.InMemoryBookRepository()
        self.event_store = InMemoryEventStore()
        self.committed = False

    def _commit(self):
        self.committed = True

    def _rollback(self):
        pass
