from event_store.domain.events import DomainEvent
from event_store.services.pub_sub import EventsListener


class InMemoryEmailService:
    def __init__(self, recipients: list):
        self.recipients = recipients
        self.sent = False

    def send(self, event: DomainEvent):
        self.sent = True


class EmailAlertsListener(EventsListener):
    def __init__(self, name, recipients):
        self.name = name
        self.service = InMemoryEmailService(recipients=recipients)
        self.recipients = []

    def warn(self, event):
        self.service.send(event=event)

    def __repr__(self):
        return f"<EmailAlertListener: {self.name}>"
