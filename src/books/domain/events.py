from dataclasses import dataclass
from uuid import UUID, uuid4
from event_store.domain.events import DomainEvent
from event_store.services.mappers import events_mapper


@dataclass
class BookCreated(DomainEvent):
    author: str
    title: str
    hash: str
    uuid: UUID = uuid4()

    def __post_init__(self):
        super(BookCreated, self).__post_init__()


@dataclass
class BookUpdated(DomainEvent):
    author: str
    title: str

    def __post_init__(self):
        super(BookUpdated, self).__post_init__()


events_mapper.register(BookCreated)
events_mapper.register(BookUpdated)
