from books.domain.book import Book
from books.domain import commands
from books.services import unit_of_work
from uuid import UUID


def add_book(cmd: commands.CreateBook, uow: unit_of_work.UnitOfWork) -> UUID:
    with uow:
        book = Book.create(cmd=cmd)
        uow.event_store.append_to_stream(
            aggregate_id=book.uuid,
            originating_version=book.version,
            events=book.changes,
            aggregate_name=book.__class__.__name__,
        )
        uow.books.add(book)
        uow.commit()
        return book.uuid


def update_book(cmd: commands.UpdateBook, uow: unit_of_work.UnitOfWork) -> None:
    with uow:
        book = uow.books.get(cmd.uuid)
        updated = book.update(cmd)
        uow.event_store.append_to_stream(
            aggregate_id=book.uuid,
            originating_version=book.version,
            events=book.changes,
            aggregate_name=book.__class__.__name__,
        )
        uow.books.update(updated)
        uow.commit()
