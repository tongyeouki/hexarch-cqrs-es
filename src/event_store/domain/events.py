import abc
from dataclasses import dataclass
from datetime import datetime
from typing import List


@dataclass
class DomainEvent(abc.ABC):
    def __post_init__(self):
        self.data: dict = {}  # Changes payload
        self.created_at: datetime = datetime.now()  # Timestamp


@dataclass
class EventStream:
    events: List[DomainEvent]
    version: int
