import abc
import json
from typing import List
from uuid import UUID, uuid4

from event_store.services.mappers import events_mapper, aggregate_mapper
from event_store.infrastructure.dao import EventStoreDao
from event_store.domain.events import DomainEvent, EventStream
from event_store.domain.aggregate import EventSourcedAggregate


class IEventStore(abc.ABC):  # pragma: no cover
    @abc.abstractmethod
    def append_to_stream(
        self,
        aggregate_id: UUID,
        originating_version: int,
        events: List[DomainEvent],
        aggregate_name: str,
    ):
        raise NotImplementedError

    @abc.abstractmethod
    def load_stream(self, aggregate_id):
        raise NotImplementedError


class InMemoryEventStore(IEventStore):
    def __init__(self):
        self.stored_events = []

    def append_to_stream(
        self,
        aggregate_id: UUID,
        originating_version: int,
        events: List[DomainEvent],
        aggregate_name: str,
    ):
        list_of_events = [
            EventStoreDao(
                uuid=uuid4(),
                created_at=event.created_at,
                version=originating_version + 1,
                name=event.__class__.__name__,
                aggregate_id=aggregate_id,
                data=json.dumps(event.data, default=str),
                aggregate=aggregate_name,
            )
            for event in events
        ]
        for event in list_of_events:
            self.stored_events.append(event)

    def load_stream(self, aggregate_id: UUID) -> EventSourcedAggregate:
        aggregate_events = [
            event for event in self.stored_events if event.aggregate_id == aggregate_id
        ]
        aggregate_name = list(set([event.aggregate for event in aggregate_events]))[0]
        events = [self._transform_events(event=event) for event in aggregate_events]
        events_stream = EventStream(events=events, version=aggregate_events[-1].version)
        aggregate_class = aggregate_mapper.get(aggregate_name=aggregate_name)
        return aggregate_class(event_stream=events_stream)

    @staticmethod
    def _transform_events(event: EventStoreDao) -> DomainEvent:
        event_class = events_mapper.get(event_name=event.name)
        data = json.loads(event.data)
        instance = event_class(**data)
        if hasattr(instance, "uuid"):
            setattr(instance, "uuid", UUID(instance.uuid))
        instance.data = data
        return instance
