# pragma: no cover

import os
import logging
from logging.handlers import RotatingFileHandler
from settings import get_config


config = get_config()

logger = logging.getLogger()
logger.setLevel(logging.CRITICAL)

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, "events.log")

format_log = logging.Formatter(
    "%(asctime)s :: %(levelname)s :: %(module)s :: %(funcName)s :: %(message)s",
    "%Y-%m-%d %H:%M:%S",
)
file_handler = RotatingFileHandler(filename, "a", 1000000, 3)
file_handler.setLevel(config.loglevel)
file_handler.setFormatter(format_log)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(format_log)
stream_handler.setLevel(logging.CRITICAL)
logger.addHandler(stream_handler)
